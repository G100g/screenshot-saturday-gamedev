import React from "react";
import styled from "styled-components";
import Layout from '../components/layout'
import {Link} from "gatsby";

import Tweet from "./../components/Tweet";
import TwitterUserLink from "./../components/TwitterUserLink";

const TweetAvatarInfos = styled.div`
    line-height: 1rem;
    margin-bottom: 0.5rem;
    text-align: center;

    a {
        text-decoration: none;
        color: white;
    }

    img {
        border-radius: 50%;
        width: 200px;
        height: 200px;
        margin: 0 auto;
        border: 7px solid white;
        background: white;
    }

    h1 {
        font-family: "Oswald", sans-serif;
        line-height: 2.1rem;
        letter-spacing: 0.15rem;
    }
    span {
        display: block;
    }

    span:last-child {
        font-weight: 300;
        font-size: 1.1rem;
        letter-spacing: 0.1rem;
    }
`;

const BackButton = styled.div`
    position: fixed;
    top: calc(50vh - 100px);
    left: 20px;
    width: 190px;
    line-height: 1rem;
    color: white;
    font-family: "Oswald", sans-serif;

    a {
        color: #ffe400;
        text-decoration: none;
    }
`;

const UserPage = ({ pageContext: data }) => {
    const tweets = data ? data.edges : [];
    const tweet = tweets[0].node;

    return (
        <Layout>
        <div>
            <TweetAvatarInfos>
                <TwitterUserLink screenName={tweet.user.screen_name}>
                    <img
                        src={tweet.user.profile_image_url_https.replace(
                            "_normal",
                            "_400x400"
                        )}
                        alt={tweet.user.name}
                    />
                    <h1>
                        <span>{tweet.user.name}</span>
                        <span>@{tweet.user.screen_name}</span>
                    </h1>
                </TwitterUserLink>
            </TweetAvatarInfos>

            <BackButton>
                <Link to="/">Go to Home</Link>
            </BackButton>

            {tweets.map((tweet, index) => (
                <Tweet key={tweet.node.id} index={index} tweet={tweet.node} />
            ))}
        </div></Layout>
    );
};

export default UserPage;
