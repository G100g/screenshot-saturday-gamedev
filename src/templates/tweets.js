import React from "react";
import Layout from "../components/layout"

import PaginationList from './../components/PaginationList';
import Tweet from './../components/Tweet';

const IndexPage = ({ pageContext: data }) => {
  const tweets = data ? data.edges : [];
  const usersGroup = data ? data.usersGroup : {};

  return (
    <Layout>
    <div>
      <PaginationList
        total={data.totalCount}
        perPage={data.perPage}
        index={data.index}
        current={data.pageNumber}
      />

      {tweets.map((tweet, index) => (
        <Tweet key={tweet.node.id} index={index} tweet={tweet.node} userTotal={usersGroup['user_' + tweet.node.user.screen_name]} />
      ))}
    </div></Layout>
  );
};

export default IndexPage;
