import React from "react";
import styled from "styled-components";
// import LazyLoad from "react-lazy-load";

const TweetMediaContainer = styled.div`
    display: flex;
    flex-direction: column;

    img,
    video {
        width: 100%;
        height: auto;
        margin: 0;
        float: left;
    }
`;

function TweetEntityMediaVideo({ poster, variants }) {
    return (
        <div>
            <video autoPlay loop muted poster={poster}>
                {variants.map(({ content_type, url }, index) => (
                    <source key={index} type={content_type} src={url} />
                ))}
            </video>
        </div>
    );
}

function TweetEntityMedia({ media }) {
    return (
        <div>
            {/* <LazyLoad offsetTop={100}> */}
            {media.type === "animated_gif" || media.type === "video" ? (
                <TweetEntityMediaVideo
                    poster={media.media_url_https}
                    variants={media.video_info.variants}
                />
            ) : (
                <img
                    src={`${media.media_url_https}`}
                    alt={media.media_url_https}
                />
            )}
            {/* </LazyLoad> */}
        </div>
    );
}

export default function({ medias, extendedMedias }) {
    if (extendedMedias && extendedMedias.media) {
        return (
            <TweetMediaContainer>
                {extendedMedias.media.map(media => (
                    <div key={media.id}>
                        <TweetEntityMedia media={media} />
                        {/* {JSON.stringify(media, null, 4)} */}
                    </div>
                ))}
            </TweetMediaContainer>
        );
    } else if (medias) {
        return (
            <TweetMediaContainer>
                {medias.map(media => (
                    <TweetEntityMedia key={media.id} media={media} />
                ))}
            </TweetMediaContainer>
        );
    }

    return null;
}
