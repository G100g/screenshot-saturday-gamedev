import React from 'react';

export default function TwitterUserLink( { screenName, children }) {

    const user_url = `https://twitter.com/${screenName}`;
    return (<a href={user_url}>
        {children}
    </a>)
}