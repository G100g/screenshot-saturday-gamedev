import React from "react";
import {Link} from "gatsby";
import styled from "styled-components";
import { media } from "../../style-utils";

const Pagination = styled.div`
    position: fixed;
    top: calc(50vh - 100px);
    left: 20px;
    width: 190px;
    line-height: 1rem;
    color: white;
    font-family: "Oswald", sans-serif;

    strong {
        font-weight: 300;
        letter-spacing: 0.2rem;
        display: block;
        border-bottom: 1px solid rgba(255, 255, 255, 0.6);
        line-height: 1.9rem;
    }

    ul {
        margin: 0;
        display: flex;
        flex-wrap: wrap;
    }
    li {
        display: inline-block;
        margin: 0;
        display: inline-block;
        padding: 0.2rem;
    }

    a {
        color: #ffe400;
        text-decoration: none;
    }

    .current a {
        color: white;
    }

    ${media.tablet`
      position: static;
      width: 50vh;
      margin: 0 auto;
      margin-bottom: 2rem;
      

      ul {
        justify-content: center;
      }

        li {
          padding: 10px;
        }
    `};
`;

const PaginationList = ({ total, perPage, index, current }) => {
    const numberOfPages =
        Math.floor(total / perPage) + (total % perPage ? 1 : 0);
    const pages = [];
    for (let i = 0; i < numberOfPages; i++) {
        pages.push(
            <li key={i} className={current === i + 1 ? "current" : ""}>
                <Link to={i === 0 ? "/" : `/page/${i + 1}`}>{i + 1}</Link>
            </li>
        );
    }

    return (
        <Pagination>
            <strong>Pages</strong>
            <ul>{pages}</ul>
        </Pagination>
    );
};

export default PaginationList;
