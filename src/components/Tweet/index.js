import React from "react";

import {Link} from "gatsby";

import LazyLoad from "react-lazy-load";
import styled from "styled-components";
import distanceInWords from "date-fns/distance_in_words";

import TweetMedia from "../TweeMedia";
import TweetLink from "../TweetLink";
import TwitterUserLink from "../TwitterUserLink";
import TweetText from "../TweetText";

import { media } from "../../style-utils";

const TweetFooterInfos = styled.div`
    display: flex;
    justify-content: space-between;

    color: #bbb;
    font-size: 0.8rem;

    > span {
        padding: 0 0.5rem;
    }

    a {
        color: #f857a6;
        text-decoration: none;
    }
`;

const TweetLikes = styled.span`
    > span {
        color: #999;
        font-weight: 600;
    }
`;

const StyleLazyLoad = styled(LazyLoad)`
    min-height: 200px;
    background-color: #f7f7f7;

    &.is-visible {
        min-height: auto;
        background-color: transparent;
    }
`;

const SingleContainer = styled.div`
    width: 50vh;
    margin: 0 auto;
    margin-bottom: 2rem;
`;

const TweetDeck = styled.div`
    background: white;
    padding: 0.5rem;
    border-radius: 5px;
    box-shadow: 0 0 13px 2px rgba(0, 0, 0, 0.3);
`;

const TweetDeckText = styled.p`
    padding: 0.85rem 0.5rem;
    margin: 0;
    font-size: 1rem;
    white-space: pre-wrap;

    a {
        color: #f857a6;
        text-decoration: none;

        &:hover {
            text-decoration: underline;
        }
    }
`;

const MoreFromUser = styled.div`
    font-size: 0.8rem;
    text-align: center;
    padding: 0.5rem 0;
    font-weight: 300;

    a {
        color: white;
        text-decoration: none;
        &:hover {
            text-decoration: underline;
        }
    }
`;

const TweetAvatarInfos = styled.div`
    font-family: "Oswald", sans-serif;
    line-height: 1rem;
    margin-bottom: 0.5rem;
    a {
        text-decoration: none;
        color: black;
        display: flex;
        align-items: center;
    }
    img {
        border-radius: 50%;
        width: 48px;
        margin: 0 0.5rem 0 0;
    }

    span {
        display: block;
    }

    span:last-child {
        font-size: 0.8rem;
        color: #f857a6;
    }
`;

const Tweet = ({ tweet, index, userTotal = {} }) => {
    tweet.user = Object.assign({ name: "", screen_name: "" }, tweet.user);
    const tweetMedia =
        (tweet.entities && tweet.entities.media) || tweet.extended_entities ? (
            <TweetMedia
                medias={tweet.entities ? tweet.entities.media : null}
                extendedMedias={tweet.extended_entities}
            />
        ) : null;

    return (
        <SingleContainer>
            <TweetDeck>
                <TweetAvatarInfos>
                    <TwitterUserLink screenName={tweet.user.screen_name}>
                        <LazyLoad height={48} width={56} offset={1000}>
                            <img
                                src={tweet.user.profile_image_url_https}
                                alt={tweet.user.name}
                            />
                        </LazyLoad>
                        <div>
                            <span>{tweet.user.name}</span>
                            <span>@{tweet.user.screen_name}</span>
                        </div>
                    </TwitterUserLink>
                </TweetAvatarInfos>
                <div>
                    {tweetMedia && (
                        <TweetLink tweet={tweet}>
                            {index > 5 ? (
                                <StyleLazyLoad offset={1000}>
                                    {tweetMedia}
                                </StyleLazyLoad>
                            ) : (
                                tweetMedia
                            )}
                        </TweetLink>
                    )}
                </div>
                <TweetDeckText>
                    {
                        <TweetText
                            content={
                                tweet.full_text ? tweet.full_text : tweet.text
                            }
                            entities={tweet.entities}
                        />
                    }
                </TweetDeckText>
                <TweetFooterInfos>
                    <span title={`${tweet.created_at}`}>
                        {distanceInWords(new Date(), tweet.created_at, {
                            addSuffix: true
                        })}
                    </span>
                    <TweetLikes>
                        <span>{tweet.favorite_count}</span> likes
                    </TweetLikes>
                </TweetFooterInfos>
            </TweetDeck>
            {userTotal > 1 && (
                <MoreFromUser>
                    <Link to={`/user/${tweet.user.screen_name}`} title={`${userTotal} tweets`}>
                        more from @{tweet.user.screen_name} 
                    </Link>
                </MoreFromUser>
            )}
        </SingleContainer>
    );
};

export default Tweet;
