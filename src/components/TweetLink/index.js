import React from "react";

export default function TweetLink({ children, tweet }) {
  const url = `https://twitter.com/${tweet.user.screen_name}/status/${tweet.id_str}`;
  return (<a href={url} target="_blank">{children}</a>)
}

