import React from "react";
import PropTypes from "prop-types";
import {Link} from "gatsby";
import Helmet from "react-helmet";

import { media } from "../style-utils";

import "./layout.css";
import "./main.css";

import styled from "styled-components";

const HeaderContainer = styled.header`
  margin-bottom: 1.45rem;
  position: fixed;
  top: 0;
  left: 0;
  padding: 0.5rem 1rem;
  background: transparent;

  h1 {
    font-family: "Oswald", sans-serif;
    text-transform: uppercase;
    line-height: 2.4rem;
  }

  span:nth-child(1) {
    font-weight: 200;
    font-size: 1.5rem;
    letter-spacing: 0.3rem;
  }

  span:nth-child(3) {
    font-size: 3rem;
    margin-left: -0.25rem;
  }

  span:nth-child(5) {
    font-weight: 200;
    font-size: 2.1rem;
    letter-spacing: 0.2rem;
  }

  ${media.tablet`
      position: static;
      text-align: center;
  `};
`;

const Header = () => (
  <HeaderContainer>
    <h1 style={{ margin: 0 }}>
      <Link
        to="/"
        style={{
          color: "white",
          textDecoration: "none"
        }}
      >
        <span>Screenshot</span>
        <br />
        <span>Saturday</span>
        <br />
        <span>#GameDev</span>
      </Link>
    </h1>
  </HeaderContainer>
);

const FooterContainer = styled.footer`
  position: fixed;
  bottom: 0;
  left: 0;
  padding: 1rem;
  margin: 0;
  background: transparent;
  width: 20vw;
  color: white;
  font-weight: 300;

  font-family: "Oswald", sans-serif;
  text-transform: uppercase;
  line-height: 1.1rem;
  letter-spacing: 0.05rem;

  a {
    color: white;
    font-weight: 400;
    text-decoration: none;
  }

  ${media.tablet`
    position: static;
    width: 90vw;
    margin: 0 auto;
    padding: 1rem;
  `}
`;

// const FooterCopy = styled.p`
//   margin-bottom: 1.45rem;
//   position: fixed;
//   bottom: 0;
//   right: 0;
//   padding: 0.5rem 1rem;
//   background: transparent;
//   padding: 1rem;
//   margin: 0;
//   font-weight: 300;

//   font-family: "Oswald", sans-serif;
//   text-transform: uppercase;
//   line-height: 1.1rem;
//   letter-spacing: 0.05rem;

//   a {
//     color: #a52759;
//     font-weight: 400;
//     text-decoration: none;
//   }

//   a:hover {
//     color: white;
//   }
// `;

const Footer = () => {
  return (
    <FooterContainer>
      <p>
        A static site generated from tweets with #screenshotsaturday #gamedev
        hashtags
      </p>
      <p>
        Made by <a href="http://g100g.net">G100g.net</a> <br /> with <a href="https://www.gatsbyjs.org/">GatsbyJS</a>
      </p>
    </FooterContainer>
  );
};

const MainContent = styled.div`
  margin: 0 auto;
  max-width: 960px;
  padding: 0px 1.0875rem 1.45rem;
  padding-top: 0;

  ${media.tablet`
    max-width: 100%;
  `}
`;

const TemplateWrapper = ({ children }) => (
  <div>
    <Helmet
      title="Screenshot Saturday Gamedev"
      meta={[
        { name: "description", content: "A Static Site Generated From Tweets With #screenshotsaturday #gamedev Hashtags" },
        { name: "keywords", content: "screenshot, saturday, gamedev" }
      ]}
    />
    <Header />
    <MainContent>{children}</MainContent>
    <Footer />
  </div>
);

TemplateWrapper.propTypes = {
  children: PropTypes.func
};

export default TemplateWrapper;
