import React from "react";

export default function TweetText({ content, entities }) {
  let { urls = [], media = [], hashtags = [], user_mentions = [] } =
    entities || {};

  if (!media) {
    media = [];
  }

  let hilightedContent = [];

  const singleEntitiesUrl = urls.map(({ url, indices }) => ({ url, indices }));
  const singleMediaUrl = media.map(({ url, indices }) => ({ url, indices }));
  const singleHashtagsUrl = hashtags.map(({ text, indices }) => ({
    text,
    indices
  }));
  const singleUserMentionsUrl = user_mentions.map(
    ({ screen_name, indices }) => ({ screen_name, indices })
  );
  var singleUrls = singleEntitiesUrl.concat(
    singleMediaUrl,
    singleHashtagsUrl,
    singleUserMentionsUrl
  );

  // Sort urls by indices
  singleUrls.sort((a, b) => a.indices[0] - b.indices[0]);

  let lastIndex = 0;
  hilightedContent = singleUrls.map(
    ({ url, text, screen_name, indices }) => {
      if (text) {
        url = `https://twitter.com/hashtag/${text}?src=hash`;
      } else if (screen_name) {
        url = `https://twitter.com/${screen_name}`;
      }

      const component = (
        <span>
          {content.substring(lastIndex, indices[0])}
          <a href={url} target="_blank">
            {content.substring(indices[0], indices[1])}
          </a>
        </span>
      );

      lastIndex = indices[1];

      return component;
    }
  );

  hilightedContent.push(content.substring(lastIndex, content.length));

  return (
    <span>
      {hilightedContent.map((text, i) => <span key={i}>{text}</span>)}
    </span>
  );
}
