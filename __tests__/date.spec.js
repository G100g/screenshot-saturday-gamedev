const {lastSaturday} = require('../utils');


test('Should return 2017-12-16', () => {

    const now = new Date(2017, 11, 16); 
    const expected = '2017-12-16';
    const result = lastSaturday(now);
    expect(result).toBe(expected);

});

test('Should return 2017-12-09', () => {

    const now = new Date(2017, 11, 15);
    const expected = '2017-12-09';
    const result = lastSaturday(now);
    expect(result).toBe(expected);

});