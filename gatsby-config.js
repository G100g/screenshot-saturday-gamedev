require("dotenv").config(".env.node")

module.exports = {
    siteMetadata: {
        title: `Gatsby Default Starter`
    },
    plugins: [
        `gatsby-plugin-styled-components`,
        `gatsby-plugin-react-helmet`,
        {
            resolve: `gatsby-source-twitter`,
            options: {
                credentials: {
                    consumer_key: process.env.TWITTER_CONSUMER_KEY,
                    consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
                    bearer_token: process.env.TWITTER_BEARER_TOKEN,
                  },
                queries: {
                    screenshots: {
                        endpoint: "search/tweets",
                        params: {
                            q: `#screenshotsaturday #gamedev -filter:retweets filter:images filter:media`,
                            tweet_mode: "extended",
                            // result_type: 'popular',
                        },
                        fetchAllResults: true
                    }
                }
            }
        },
        {
            resolve: `gatsby-plugin-google-analytics`,
            options: {
                trackingId: "UA-320104-12",
                // Setting this parameter is optional
                anonymize: true
            }
        }
    ]
};
