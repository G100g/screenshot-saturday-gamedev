var isSaturday = require('date-fns/is_saturday')
var isSunday = require('date-fns/is_sunday')
var setDay = require('date-fns/set_day');
var format = require('date-fns/format')
var endOfWeek = require('date-fns/end_of_week')
var subWeeks = require('date-fns/sub_weeks')

module.exports = {
    lastSaturday(date) {
        if (!isSaturday(date)   && !isSaturday(date)) {
            date = subWeeks(date, 1);
        }
        return format(endOfWeek(date), 'YYYY-MM-DD');
    }
}