// @ts-check

// const {
//     GraphQLObjectType,
//     GraphQLBoolean,
//     GraphQLString,
//     GraphQLInt,
//     GraphQLFloat,
//     GraphQLEnumType
//   } = require(`graphql`);

// exports.setFieldsOnGraphQLNodeType = ({ type }) => {
//     console.log(type.name)
//   if (type.name !== `Tweet`) {
//     return {};
//   }

//   return {
//     text: {
//       type: GraphQLString,
//       defaultValue: `No tweet text anymore`,
//       resolve(text) {
//         console.log(arguments);
//         return "";
//       }
//     },
//   };
// };
const path = require("path");
const co = require("co");

function createIndexPage(createPage, graphql, perPage = 50, index) {
  // const skip = index + perPage;
  const pageNumber = Math.ceil(index / perPage) + 1;

  // return new Promise((resolve, reject) => {
  const query = `{
    allTwitterSearchTweetsScreenshots(limit: ${perPage}, skip: ${index}, sort: { fields: [favorite_count], order: DESC }) {
        pageInfo {
            hasNextPage
          }
          totalCount
      edges {
        node {
          id
          id_str
          
          full_text
          created_at
          entities {
            urls {
              url
              expanded_url
              display_url
              indices
            }
            media {
              id
              media_url
              media_url_https
              indices
              url
            }
            hashtags {
              text
              indices
            }
            user_mentions {
              screen_name
              indices
            }
          }
          extended_entities {
            media {
              id
              type
              media_url_https
              video_info {
                variants {
                  bitrate
                  content_type
                  url
                }
              }
            }
          }
          favorite_count
          user {
            id
            name
            screen_name
            url
            profile_image_url_https
          }
        }
      }
      group(field: user___screen_name) {
        fieldValue
        totalCount
      }
    }}
    `;
  
    const queryUsersGroup = `{
      allTwitterSearchTweetsScreenshots {       
        group(field: user___screen_name) {
          fieldValue
          totalCount
        }
      }}
      `;

  return Promise.all([
    graphql(query),
    graphql(queryUsersGroup)
  ])
  
  
    .then(([result, groupResult]) => {
    if (
      result &&
      result.data &&
      result.data.allTwitterSearchTweetsScreenshots &&
      result.data.allTwitterSearchTweetsScreenshots.edges
    ) {

      // Create Users Tweet Total Count Group
      const usersGroup = groupResult.data.allTwitterSearchTweetsScreenshots.group.reduce((r, v) => {
        r[`user_${v.fieldValue}`] = v.totalCount;
                return r;

      }, {});

      createPage({
        path: index === 0 ? "/" : `/page/${pageNumber}`,
        component: path.resolve(`./src/templates/tweets.js`),
        context: {
          edges: result.data.allTwitterSearchTweetsScreenshots.edges,
          totalCount: result.data.allTwitterSearchTweetsScreenshots.totalCount,
          perPage: perPage,
          index,
          pageNumber,
          usersGroup
        }
      });
      // result.data.allTwitterSearchTweetsScreenshots.edges.map(({ node }) => {
      // })
      return result.data.allTwitterSearchTweetsScreenshots.pageInfo.hasNextPage;
    }
    return false;
  });
}

function createUserPages(createPage, graphql) {
  const query = `{    
      allTwitterSearchTweetsScreenshots {
        group(field: user___screen_name) {
          fieldValue
          edges {
            node {
              id
              id_str
              
              full_text
              created_at
              entities {
                urls {
                  url
                  expanded_url
                  display_url
                  indices
                }
                media {
                  id
                  media_url
                  media_url_https
                  indices
                  url
                }
                hashtags {
                  text
                  indices
                }
                user_mentions {
                  screen_name
                  indices
                }
              }
              extended_entities {
                media {
                  id
                  type
                  media_url_https
                  video_info {
                    variants {
                      bitrate
                      content_type
                      url
                    }
                  }
                }
              }
              favorite_count
              user {
                id
                name
                screen_name
                url
                profile_image_url_https
              }
            }
          }
        } 
      }
    }
    `;

  return graphql(query).then(result => {      
      if (
        result &&
        result.data &&
        result.data.allTwitterSearchTweetsScreenshots &&
        result.data.allTwitterSearchTweetsScreenshots.group
      ) {
        result.data.allTwitterSearchTweetsScreenshots.group.forEach(({
          fieldValue: screen_name,
          edges
        }) => {
          createPage({
            path: `/user/${screen_name}`,
            component: path.resolve(`./src/templates/user-tweets.js`),
            context: {
              edges: edges,
              totalCount: 0,
              perPage: 0,
              index: 0,
              pageNumber: 0
            }
          });
        });
      }
    })
    .catch(err => {
      console.error("User pages not created", err);
    });
}

exports.createPages = ({
  graphql,
  actions
}) => {
  const {
    createPage
  } = actions;

  return co(function* () {
    let index = 0;
    const perPage = 50;

    let hasNextPage = yield createIndexPage(
      createPage,
      graphql,
      perPage,
      index
    );

    while (hasNextPage) {
      index += perPage;
      hasNextPage = yield createIndexPage(createPage, graphql, perPage, index);
    }

    yield createUserPages(createPage, graphql);
  });
};

/*

  {
    allTwitterSearchTweetsScreenshots {
      group(field: user___screen_name) {
        fieldValue
      } 
    }
}

  allTwitterSearchTweetsScreenshots(filter: { user: { screen_name: { eq: "NowwaCorp"}}}) {
      edges {
        node {
          id
        }
      }
    }

    */
